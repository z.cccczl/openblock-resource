# openblock-resource

![](https://img.shields.io/travis/com/openblockcc/openblock-resource) ![](https://img.shields.io/github/license/openblockcc/openblock-resource)

Provide a local resource server for openblock.

### Instructions

```bash
npm install
npm start
```

To test update funciton, run:

```bash
npm test
```
